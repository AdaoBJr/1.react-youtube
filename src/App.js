import React, { useState } from 'react';
import YouTube from 'react-youtube';
const getYouTubeID = require('get-youtube-id');

function App() {
  const [id, setId] = useState('');

  const opts = {
    height: '390',
    width: '640',
    playerVars: {
      autoplay: 0,
    },
  };

  return (
    <div>
      <input
        type="text"
        onChange={(e) => {
          setId(getYouTubeID(e.target.value));
        }}
        placeholder="URL..."
      />

      <YouTube videoId={id} opts={opts} />
    </div>
  );
}

export default App;
